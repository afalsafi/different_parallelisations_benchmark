#!/usr/bin/env python
# coding: utf-8

# In[1]:


import csv
import matplotlib.pyplot as plt


# In[2]:


with open('GEMM_output_TBB_size_2000.csv') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    line_count = 0
    p_count = []
    times = []
    reductions_tbb = []
    for i, row in enumerate(csv_reader):
        if i != 0:
            times.append(float(row[3])) 
            p_count.append(int(row[1])) 
    for i, time in enumerate(times):
        reductions_tbb.append(times[0]/time)
    plt.plot(p_count, reductions_tbb, label = "TBB")


# In[3]:


with open('GEMM_output_OMP_size_2000.csv') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    line_count = 0
    p_count = []
    times = []
    reductions_omp = []
    for i, row in enumerate(csv_reader):
        if i != 0:
            times.append(float(row[3])) 
            p_count.append(int(row[1])) 
    for i, time in enumerate(times):
        reductions_omp.append(times[0]/time)
    plt.plot(p_count, reductions_omp, label = "OMP")


# In[4]:


with open('GEMM_output_THR_size_2000.csv') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    line_count = 0
    p_count = []
    times = []
    reductions_thr = []
    for i, row in enumerate(csv_reader):
        if i != 0:
            times.append(float(row[3])) 
            p_count.append(int(row[1])) 
    for i, time in enumerate(times):
        reductions_thr.append(times[0]/time)
    plt.plot(p_count, reductions_thr, label = "std thread")


# In[5]:


with open('GEMM_output_EXC_size_2000.csv') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    line_count = 0
    p_count = []
    times = []
    reductions_cpp = []
    for i, row in enumerate(csv_reader):
        if i != 0:
            times.append(float(row[3])) 
            p_count.append(int(row[1])) 
    for i, time in enumerate(times):
        reductions_cpp.append(times[0]/time)
    plt.plot(p_count, reductions_cpp, label="c++ execution")


# In[6]:


plt.figure(figsize = (12, 8))
ax = plt.subplot(111)
plt.plot(p_count, reductions_omp,'d:', linewidth = 2.5, label = "OMP", markersize = 12)
plt.plot(p_count, reductions_tbb, 'x--',  linewidth = 2.5, label = "TBB", markersize = 12)
ax.plot(p_count, reductions_cpp, 'o-.', linewidth = 2.5, label="c++ execution", markersize = 12)
plt.plot(p_count, reductions_thr, 'P-', linewidth = 2.5, label = "std thread", markersize = 12)
plt.grid()
plt.legend(fontsize =18)


# In[ ]:




