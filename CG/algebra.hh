
/* -------------------------------------------------------------------------- */
#ifndef ALGEBRA_HH
#define ALGEBRA_HH

/* -------------------------------------------------------------------------- */
#include "/opt/intel/compilers_and_libraries_2019.5.281/linux/pstl/include/pstl/algorithm"
#include "/opt/intel/compilers_and_libraries_2019.5.281/linux/pstl/include/pstl/execution"
#include <cmath>
#include <functional>
#include <future>
#include <memory>
#include <mutex>
#include <numeric>
#include <omp.h>
#include <sstream>
#include <tbb/tbb.h>
#include <thread>
#include <vector>
/* -------------------------------------------------------------------------- */
#include "../ThreadPool_ext/ThreadPool.h"
#include "mat.hh"
/* -------------------------------------------------------------------------- */
using Real = double;
using Mat_t = Mat<Real>;
using Vec_t = std::vector<Real>;

/* -------------------------------------------------------------------------- */
// classes' prototypes:
template <class T> class Algebra_SEQ;
template <class T> class Algebra_OMP;
template <class T> class Algebra_TBB;
template <class T> class Algebra_EXC;

/* -------------------------------------------------------------------------- */
// classes' implementation

template <class T> class Algebra_SEQ {
public:
  // Matrix times vector
  inline auto inner_product(const Vec_t &u, const Vec_t &v) -> T {
    T ret_val = 0.0;
    for (int i{0}; i < u.size(); ++i) {
      ret_val += u[i] * v[i];
    }
    return ret_val;
  }

  // Inner product of U and V
  inline auto matrix_times_vector(const Mat_t &A, const Vec_t &b) -> Vec_t {
    Vec_t ret_vec(b.size(), 0.0);
    for (int i = 0; i < A.size()[0]; ++i) {
      for (int j = 0; j < b.size(); ++j) {
        ret_vec[i] += A(i, j) * b[j];
      }
    }
    return ret_vec;
  }

  // Vector norm
  inline auto vec_norm(const Vec_t &v) -> double {
    return std::sqrt(inner_product(v, v));
  }

  // Vector combination
  inline auto vec_comb(const T &a, const Vec_t &u, const T &b, const Vec_t &v)
      -> Vec_t {
    Vec_t ret_vec(u.size(), 0.0);
    for (int i = 0; i < u.size(); ++i) {
      ret_vec[i] = a * u[i] + b * v[i];
    }
    return ret_vec;
  }
  int n_proc {1};
};

/* -------------------------------------------------------------------------- */
template <class T> class Algebra_OMP {
public:
  // constructor
  Algebra_OMP(int n_proc) : n_proc{n_proc} { omp_set_num_threads(n_proc); };

  // Matrix times vector
  inline auto inner_product(const Vec_t &u, const Vec_t &v) -> T {
    T ret_val = 0.0;
#pragma omp parallel for reduction(+ : ret_val)
    for (int i = 0; i < u.size(); ++i) {
      ret_val += u[i] * v[i];
    }
    return ret_val;
  }

  // Inner product of U and V
  inline auto matrix_times_vector(const Mat_t &A, const Vec_t &b) -> Vec_t {
    Vec_t ret_vec(b.size(), 0.0);
#pragma omp parallel for
    for (int i = 0; i < A.size()[0]; ++i) {
      for (int j = 0; j < b.size(); ++j) {
        ret_vec[i] += A(i, j) * b[j];
      }
    }
    return ret_vec;
  }

  // Vector norm
  inline auto vec_norm(const Vec_t &v) -> double {
    return std::sqrt(inner_product(v, v));
  }

  // Vector combination
  inline auto vec_comb(const T &a, const Vec_t &u, const T &b, const Vec_t &v)
      -> Vec_t {
    Vec_t ret_vec(u.size(), 0.0);
#pragma omp parallel for
    for (int i = 0; i < u.size(); ++i) {
      ret_vec[i] = a * u[i] + b * v[i];
    }
    return ret_vec;
  }

  int n_proc;
};

/* -------------------------------------------------------------------------- */
template <class T> class Algebra_TBB {

public:
  // constructor
  Algebra_TBB(int n_proc = 1) : n_proc{n_proc} {}
  // Matrix times vector
  inline auto inner_product(const Vec_t &u, const Vec_t &v) -> T {
    return tbb::parallel_reduce(
        tbb::blocked_range<int>(0, u.size()), 0.0,
        [&](const tbb::blocked_range<int> &range, double init) -> double {
          for (int i = range.begin(); i < range.end(); ++i) {
            init += u[i] * v[i];
          }
          return init;
        },
        [](double x, double y) -> double { return x + y; });
  }

  // Inner product of U and V
  inline auto matrix_times_vector(const Mat_t &A, const Vec_t &b) -> Vec_t {
    Vec_t ret_vec(b.size(), 0.0);
    auto _mul = [&](int i) {
      for (int j = 0; j < b.size(); ++j) {
        ret_vec[i] += A(i, j) * b[j];
      }
    };
    tbb::parallel_for(tbb::blocked_range<int>(0, b.size()),
                      [&](const tbb::blocked_range<int> &range) {
                        for (int i = range.begin(); i < range.end(); ++i) {
                          _mul(i);
                        }
                      });
    return ret_vec;
  }

  // Vector norm
  inline auto vec_norm(const Vec_t &v) -> double {
    return std::sqrt(inner_product(v, v));
  }

  // Vector combination
  inline auto vec_comb(const T &a, const Vec_t &u, const T &b, const Vec_t &v)
      -> Vec_t {
    Vec_t ret_vec(u.size(), 0.0);
    tbb::parallel_for(tbb::blocked_range<int>(0, u.size()),
                      [&](const tbb::blocked_range<int> &range) {
                        for (int i = range.begin(); i < range.end(); ++i) {
                          ret_vec[i] = a * u[i] + b * v[i];
                        }
                      });
    return ret_vec;
  }
  int n_proc;
};

/* -------------------------------------------------------------------------- */
template <class T> class Algebra_EXC {
public:
  // constructor
  Algebra_EXC(int size, int n_proc) : n_proc{n_proc} {
    int count{0};
    this->nums.resize(size);
    for (auto &&num : this->nums) {
      num = count;
      count++;
    }
  }

  inline auto inner_product(const Vec_t &u, const Vec_t &v) -> T {
    return std::transform_reduce(std::execution::par_unseq, this->nums.begin(),
                                 this->nums.end(), T{0.0}, std::plus<>(),
                                 [&](int i) { return u[i] * v[i]; });
  }

  // Matrix times vector
  inline auto matrix_times_vector(const Mat_t &A, const Vec_t &b) -> Vec_t {
    Vec_t ret_vec(b.size(), 0.0);

    auto _mul = [&](int i) {
      for (int j = 0; j < b.size(); ++j) {
        ret_vec[i] += A(i, j) * b[j];
      }
    };

    std::for_each(std::execution::par_unseq, this->nums.begin(),
                  this->nums.end(), [&](int &num) { _mul(num); });

    return ret_vec;
  }

  // Vector norm
  inline auto vec_norm(const Vec_t &v) -> double {
    return std::sqrt(inner_product(v, v));
  }
  inline auto vec_comb(const T &a, const Vec_t &u, const T &b, const Vec_t &v)
      -> Vec_t {
    Vec_t ret_vec(u.size(), 0.0);
    auto _comb = [&](int i) { ret_vec[i] = a * u[i] + b * v[i]; };
    std::for_each(std::execution::par_unseq, this->nums.begin(),
                  this->nums.end(), [&](int &num) { _comb(num); });
    return ret_vec;
  }
  int n_proc;
  std::vector<int> nums;
  std::mutex mutex;
};

/* --------------------------------------------------------------------------*/
template <class T> class Algebra_THR {
public:
  // constructor
  Algebra_THR(int n_proc, int size) : n_proc{n_proc}, thread_pool(n_proc) {
    int size_thr;
    start_thrs.push_back(0);
    for (int n = 0; n < n_proc; ++n) {
      n < size % n_proc ? size_thr = size / n_proc + 1
                        : size_thr = size / n_proc;
      start_thrs.push_back(start_thrs[start_thrs.size() - 1] + size_thr);
      end_thrs.push_back(start_thrs[start_thrs.size() - 1]);
    }
    start_thrs.pop_back();
  };

  // Inner product of U and V
  inline auto inner_product(const Vec_t &u, const Vec_t &v) -> T {

    std::vector<std::future<double>> rets;
    //task function:
    auto _prod = [&](int n) {
      T sum{0.0};
      for (int i = start_thrs[n]; i < end_thrs[n]; ++i) {
        sum += u[i] * v[i];
      }
      return sum;
    };

    // creating threads
    for (int n = 0; n < this->n_proc; ++n) {
      rets.emplace_back(thread_pool.enqueue(_prod, n));
    }

    return std::transform_reduce(rets.begin(), rets.end(), T{0.0},
                                 std::plus<>(),
                                 [](std::future<T> &ret) { return ret.get(); });
  }

  // Matrix times vector
  inline auto matrix_times_vector(const Mat_t &A, const Vec_t &b) -> Vec_t {
    Vec_t ret_vec(b.size(), 0.0);
    // task function:
    auto _mul = [&](int n) {
      for (int i = start_thrs[n]; i < end_thrs[n]; ++i) {
        for (int j = 0; j < b.size(); ++j) {
          ret_vec[i] += A(i, j) * b[j];
        }
      }
      return true;
    };
    // creating tasks
    for (int n = 0; n < this->n_proc; ++n) {
      tmp_futures.emplace_back(thread_pool.enqueue(_mul, n));
    }
    for (auto &&future : tmp_futures) {
      future.wait();
    }
    tmp_futures.clear();
    return ret_vec;
  }

  // Vector norm
  inline auto vec_norm(const Vec_t &v) -> double {
    return std::sqrt(inner_product(v, v));
  }

  // Vector combination
  inline auto vec_comb(const T &a, const Vec_t &u, const T &b, const Vec_t &v)
      -> Vec_t {
    Vec_t ret_vec(u.size(), 0.0);

    // thread task function:
    auto _comb = [&](int n) {
      for (int i = start_thrs[n]; i < end_thrs[n]; ++i) {
        ret_vec[i] = a * u[i] + b * v[i];
      }
      return true;
    };

    for (int n = 0; n < this->n_proc; ++n) {
      tmp_futures.emplace_back(thread_pool.enqueue(_comb, n));
    }
    for (auto &&future : tmp_futures) {
      future.wait();
    }
    tmp_futures.clear();
    return ret_vec;
  }

  int n_proc;
  ThreadPool thread_pool;
  std::vector<std::future<bool>> tmp_futures{};
  std::vector<int> start_thrs;
  std::vector<int> end_thrs;
};

#endif /* ALGEBRA_HH */
