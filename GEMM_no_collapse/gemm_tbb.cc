/* -------------------------------------------------------------------------- */
#include <chrono>
#include <iostream>
#include <memory>
#include <sstream>
#include <sys/time.h>
#include <tbb/tbb.h>
/* -------------------------------------------------------------------------- */
#include "mat.hh"
#include "print_outputs.hh"
/* -------------------------------------------------------------------------- */

int main(int /*argc*/, char *argv[]) {
  std::stringstream args1(argv[1]);
  std::stringstream args2(argv[2]);
  std::stringstream args3(argv[3]);
  int size1, size2, n_proc;
  args1 >> size1;
  args2 >> size2;
  args3 >> n_proc;

  auto A = Mat_t(size1, size2, 1.0);
  auto B = Mat_t(size2, size1, 2.0);
  auto C = Mat_t(size1, size1, 0.0);

  // Matrix multiplication inline function
  auto _mul = [&](int i) {
    for (int k = 0; k < size2; k++) {
      for (int j = 0; j < size1; j++) {
        C(i, j) += A(i, k) * B(k, j);
      }
    }
  };

  std::string method = "TBB";
  if (n_proc == 1) {
    print_header(size1, size2, method);
  }

  tbb::task_scheduler_init init(n_proc);
  // parallel loop using TBB
  auto &&start = clk::now();
  start = clk::now();
  tbb::parallel_for(0, size1, [&](int &i) { _mul(i); });
  auto &&end = clk::now();
  second time = end - start;
  print_outputs_file(time, size1, size2, method, C, n_proc);

  return 0;
}
