
/* -------------------------------------------------------------------------- */
#include "cg_solver.hh"
/* -------------------------------------------------------------------------- */
const double eps = 1.0e-10; // interpretation of "zero"

int main(int argc, char *argv[]) {
  std::stringstream args2(argv[1]);
  std::stringstream args1(argv[2]);
  int size;
  int n_proc;
  args2 >> size;
  args1 >> n_proc;

  Vec_t B(size);
  auto A = Mat_t(size, size, 0.0);
  for (int i = 0; i < size; ++i) {
    A(i, i) = i + 1;
    B[i] = size - i;
  }

  std::string method = "TBB";
  if (n_proc == 1) {
    print_header(size, method);
  }

  const int p = tbb::task_scheduler_init::default_num_threads();

  tbb::task_scheduler_init init(n_proc);
  auto alg = Algebra_TBB<Real>(n_proc);
  std::cout << "nb of procs :" << alg.n_proc << "   " << std::flush;
  cg_solver(A, B, alg, eps, method);
  std::cout << "DONE" << std::endl;
  return 0;
}
