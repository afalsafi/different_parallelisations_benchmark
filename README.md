# different_parallelisations_benchmark

This Repo is made to contain my project for the Parallel programming course of 2019 fall semester.

The main objective of the project is comparisong of performance of 4 thread based Parallelisation schemes, namely:

1. OpenMP
2. Intel Thread Building Block (TBB)
3. C++17 parallel execution policy
4. Manually made threads using std::threads.

The problems chosen  as the sample problems are GEMM (generall matrix-matrix multiplication) problems with variant sizes.

Running Files in `GEMM` folder gives you the time needed for doing the matrix-matrix multiplications.

A brief presentation on the repo content is available in `Presentation` folder.
