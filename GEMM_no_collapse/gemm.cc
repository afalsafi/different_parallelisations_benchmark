/* -------------------------------------------------------------------------- */
#include <chrono>
#include <iostream>
#include <memory>
#include <sstream>
#include <sys/time.h>
#include <tbb/tbb.h>
/* -------------------------------------------------------------------------- */
#include "mat.hh"
#include "print_outputs.hh"
/* -------------------------------------------------------------------------- */
void matrix_matrix_mul(int size1, int size2, const Mat_t &A, const Mat_t &B,
                       Mat_t &C) {
  for (int i = 0; i < size1; i++) {
    for (int k = 0; k < size2; k++) {
      for (int j = 0; j < size1; j++) {
        C(i, j) += A(i, k) * B(k, j);
      }
    }
  }
}

/* --------------------------------------------------------------------------
 */
int main(int /*argc*/, char *argv[]) {
  std::stringstream args1(argv[1]);
  std::stringstream args2(argv[2]);
  int size1, size2, n_proc;
  args1 >> size1;
  args2 >> size2;

  auto A = Mat_t(size1, size2, 1.0);
  auto B = Mat_t(size2, size1, 2.0);
  auto C = Mat_t(size1, size1, 0.0);

  // Sequential loops
  auto &&start = clk::now();
  matrix_matrix_mul(size1, size2, A, B, C);
  std::string method = "SEQ";
  print_header(size1, size2, method);
  auto &&end = clk::now();
  second time = end - start;
  print_outputs_file(time, size1, size2, method, C, 1);
  return 0;
}
