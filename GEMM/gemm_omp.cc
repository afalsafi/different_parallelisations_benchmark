/* -------------------------------------------------------------------------- */
#include <chrono>
#include <iostream>
#include <memory>
#include <sstream>
#include <sys/time.h>
#include <tbb/tbb.h>
/* -------------------------------------------------------------------------- */
#include "mat.hh"
#include "print_outputs.hh"
/* -------------------------------------------------------------------------- */

int main(int /*argc*/, char *argv[]) {

  std::stringstream args(argv[1]);
  int N;
  args >> N;

  int size = 2000;

  // std::unique_ptr<Mat<double>> A;
  auto A = Mat_t(size, size, 1.0);
  auto B = Mat_t(size, size, 2.0);
  auto C = Mat_t(size, size, 0.0);

  // Matrix multiplication inline function
  auto _mul = [&](int i) {
    for (int j = 0; j < size; j++) {
      for (int k = 0; k < size; k++) {
        C(i, j) += A(i, k) * B(k, j);
      }
    }
  };

  // Matrix multiplication inline function
  auto _mulij = [&](int i, int j) {
    for (int k = 0; k < size; k++) {
      C(i, j) += A(i, k) * B(k, j);
    }
  };

  // one parallel loop using omp
  auto &&start = clk::now();
#pragma omp parallel for num_threads(N)
  for (int i = 0; i < size; i++) {
    _mul(i);
  }
  auto &&end = clk::now();
  second time = end - start;
  print_outputs(time, size, 0, "Sequential", C);

  // two nested parallel loops using omp
  A.fill_num(1.0);
  B.fill_num(2.0);
  C.fill_num(0.0);

  start = clk::now();
#pragma omp parallel for collapse(2) num_threads(N)
  for (int i = 0; i < size; i++) {
    for (int j = 0; j < size; j++) {
      _mulij(i, j);
    }
  }
  end = clk::now();
  time = end - start;
  print_outputs(time, size, 2, "omp", C);

  return 0;
}
