
/* -------------------------------------------------------------------------- */
#include "cg_solver.hh"
/* -------------------------------------------------------------------------- */
const double eps = 1.0e-10; // interpretation of "zero"

int main(int argc, char *argv[]) {
  auto alg = Algebra_SEQ<Real>();
  const int size = 5000;
  //
  Vec_t B(size);
  auto A = Mat_t(size, size, 0.0);
  for (int i = 0; i < size; ++i) {
    A(i, i) = i + 1;
    B[i] = size - i;
  }
  std::string method = "SEQ";
  print_header(size, method);
  cg_solver(A, B, alg, eps, method);
  return 0;

  return 0;
}
