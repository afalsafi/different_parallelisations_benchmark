/* -------------------------------------------------------------------------- */
#include <algorithm>
#include <chrono>
#include <cmath>
//#include <eigen3/Eigen/Core>
#include <array>
#include <eigen3/Eigen/IterativeLinearSolvers>
#include <eigen3/Eigen/Sparse>
#include <fstream>
#include <iostream>
#include <numeric>
#include <string>
#include <vector>

/* -------------------------------------------------------------------------- */
#include "print_outputs.hh"
/* -------------------------------------------------------------------------- */
const double eps = 1.0e-10; // interpretation of "zero"

int main(int argc, char *argv[]) {
  int size = 1000;

  Eigen::SparseMatrix<double> A(size, size);
  A = Eigen::MatrixXd::Random(size, size).sparseView(0.5, 1);
  A = A.transpose() * A;
  Eigen::VectorXd B(size);

  for (int i = 0; i < size; ++i) {
    B(i) = size - i;
  }

  // std::ofstream matrix_file;
  // matrix_file.open("matrix_file.txt");
  // matrix_file << A << std::endl;
  // matrix_file.close();

  // std::array<int, 6> n_procs = {24, 12, 6, 3, 2, 1};
  const int p = omp_get_max_threads();
  for (int i = 1; i <= p; ++i) {
    // for (auto &&n_proc : n_procs) {
    Eigen::VectorXd X(size), B(size);
    omp_set_num_threads(i);
    Eigen::initParallel();
    Eigen::setNbThreads(i);
    std::cout << Eigen::nbThreads() << std::endl;
    auto &&start = clk::now();
    Eigen::ConjugateGradient<Eigen::SparseMatrix<double>,
                             Eigen::Upper | Eigen::Lower>
        cg;
    cg.compute(A);
    X = cg.solve(B);
    auto &&end = clk::now();
    second time = end - start;
    print_outputs_file(time, size, "eigen", cg.iterations(), cg.error(), i);
  }
  return 0;
}
