#include <chrono>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <memory>
/* -------------------------------------------------------------------------- */
#include "mat.hh"
/* -------------------------------------------------------------------------- */

typedef std::chrono::high_resolution_clock clk;
typedef std::chrono::duration<double> second;
using Mat_t = Mat<double>;
/* -------------------------------------------------------------------------- */

void print_outputs(second time, int size, std::string method, int k = 0,
                   double err = 0, double eps = 0, int n_proc = 1) {

  std::cout << method << std::endl;
  std::cout << n_proc << std::endl;
  std::cout << "problem size   :" << size << std::endl;
  std::cout << "compute time   : " << time.count() << " [s]" << std::endl;
  if (k != 0) {
    std::cout << "Solves AX = B "
              << "in " << k << " steps" << std::endl;
    if (err < eps) {
      std::cout << "The found matrix X satisfy the equation AX = B"
                << std::endl;
    } else {
      std::cout << "The found matrix X does not satisfy the equation AX = B"
                << std::endl;
    }
    std::cout << "err   : " << err << std::endl;
  }
}

/* -------------------------------------------------------------------------- */
void print_outputs_aligned(second time, int size, std::string method, int k = 0,
                           double err = 0, double eps = 0, int n_proc = 1) {

  std::cout << std::setw(7) << method << ",";
  std::cout << std::setw(11) << n_proc << ",";
  std::cout << std::setw(9) << size << ",";
  std::cout << std::setw(9) << time.count() << ",";
  if (k != 0) {
    std::cout << std::setw(9) << k << ",";
    // if (err < eps) {
    //   std::cout << "The found matrix X satisfy the equation AX = B"
    //             << std::endl;
    // } else {
    //   std::cout << "The found matrix X does not satisfy the equation AX = B"
    //             << std::endl;
    // }
    std::cout << std::setw(19) << err << std::endl;
  }
}

/* -------------------------------------------------------------------------- */
void print_outputs_file(second time, int size, std::string method, int k = 0,
                        double err = 0.0, int n_proc = 1) {
  std::ofstream outfile;
  outfile.open("output_" + method + "_size_" + std::to_string(size) + ".csv",
               std::ios::out | std::ios::app);
  outfile << std::setw(7) << method << ",";
  outfile << std::setw(11) << n_proc << ",";
  outfile << std::setw(9) << size << ",";
  outfile << std::setw(9) << time.count() << ",";
  if (k != 0) {
    outfile << std::setw(9) << k << ",";
    outfile << std::setw(19) << err << std::endl;
    outfile.close();
  }
}

/* -------------------------------------------------------------------------- */
void print_header(int size, std::string method) {
  std::ofstream outfile;
  outfile.open("output_" + method + "_size_" + std::to_string(size) + ".csv",
               std::ios::out);
  outfile << std::setw(20) << "nb of procs,";
  outfile << std::setw(10) << "size,";
  outfile << std::setw(10) << "time,";
  outfile << std::setw(10) << "nb steps,";
  outfile << std::setw(20) << "cg_err" << std::endl;
  outfile.close();
}
