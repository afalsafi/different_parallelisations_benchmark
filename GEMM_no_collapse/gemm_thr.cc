/* -------------------------------------------------------------------------- */
#include <algorithm>
#include <chrono>
#include <iostream>
#include <memory>
#include <omp.h>
#include <sstream>
#include <sys/time.h>
#include <tbb/tbb.h>
#include <thread>
/* -------------------------------------------------------------------------- */
#include "mat.hh"
#include "print_outputs.hh"
/* -------------------------------------------------------------------------- */

int main(int /*argc*/, char *argv[]) {
  std::stringstream args1(argv[1]);
  std::stringstream args2(argv[2]);
  std::stringstream args3(argv[3]);
  int size1, size2, n_proc;
  args1 >> size1;
  args2 >> size2;
  args3 >> n_proc;

  auto A = Mat_t(size1, size2, 1.0);
  auto B = Mat_t(size2, size1, 2.0);
  auto C = Mat_t(size1, size1, 0.0);

  // Matrix multiplication inline function
  auto _mul = [&](int p, int n, int size_thr) {
    for (int i = n * size1 / p; i < n * size1 / p + size_thr; i++) {
      for (int k = 0; k < size2; k++) {
        for (int j = 0; j < size1; j++) {
          C(i, j) += A(i, k) * B(k, j);
        }
      }
    }
  };
  std::vector<std::thread> thread_list;
  std::string method = "THR";
  if (n_proc == 1) {
    print_header(size1, size2, method);
  }
  auto &&start = clk::now();
  for (int n = 0; n < n_proc; ++n) {
    int size_thr;
    n < size1 % n_proc ? size_thr = size1 / n_proc + 1
                       : size_thr = size1 / n_proc;
    thread_list.push_back(std::thread(_mul, n_proc, n, size_thr));
  }
  std::for_each(thread_list.begin(), thread_list.end(),
                std::mem_fn(&std::thread::join));
  auto &&end = clk::now();
  second time = end - start;

  print_outputs_file(time, size1, size2, method, C, n_proc);
  return 0;
}
