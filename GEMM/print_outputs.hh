#include <chrono>
#include <memory>
#include <iostream>
/* -------------------------------------------------------------------------- */
#include "mat.hh"
/* -------------------------------------------------------------------------- */

typedef std::chrono::high_resolution_clock clk;
typedef std::chrono::duration<double> second;
using Mat_t = Mat<double>;
/* -------------------------------------------------------------------------- */

void print_outputs(second time, int size, int par_loops,std::string method,
                   Mat_t &C) {

  std::cout << method << std::endl;
  std::cout << "parallel loops : " << par_loops << std::endl;
  std::cout << "compute time   : " << time.count() << " [s]" << std::endl;
  std::cout << "Performance    : "
            << (((double)size * (double)size * (double)size * 2.0) /
                ((time.count()) * 1.e9))
            << " [GF/s]" << std::endl;
  std::cout << "Verification   : " << C.verify() << std::endl << std::endl;
}
