#ifndef MAT_HH
#define MAT_HH

/* -------------------------------------------------------------------------- */
#include <array>
#include <memory>
#include <vector>
/* -------------------------------------------------------------------------- */

template <typename T> class Mat {

public:
  // constructor
  Mat(int m = 0, int n = 0);

  // constructor
  Mat(int m, int n, T num);

  /// access the value [i][j] of the mat
  inline T &operator()(int i, int j) { return m_storage[i * m_size[1] + j]; }

  inline const T &operator()(int i, int j) const {
    return m_storage[i * m_size[1] + j];
  }

  // resize the mat
  void resize(int m, int n);

  /// set the mat to 0
  void clear();

  /// fill the mat with a number:
  void fill_num(T num);

  /// verify the output of the multiplication by summing up the values of the
  /// storage
  bool verify();

  // returns the size of the matrix
  std::array<int, 2> size() const { return this->m_size; }

protected:
  std::array<int, 2> m_size;
  std::vector<T> m_storage;
};

#endif /* MAT_HH */
