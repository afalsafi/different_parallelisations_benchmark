/* -------------------------------------------------------------------------- */
#include "mat.hh"
/* -------------------------------------------------------------------------- */
#include <algorithm>
/* -------------------------------------------------------------------------- */

/* -------------------------------------------------------------------------- */
template <typename T>
Mat<T>::Mat(int m, int n) : m_size{m, n}, m_storage(m * n) {
  this->clear();
}

/* -------------------------------------------------------------------------- */
template <typename T>
Mat<T>::Mat(int m, int n, T num) : m_size{m, n}, m_storage(m * n) {
  this->fill_num(num);
}

/* -------------------------------------------------------------------------- */
template <typename T> void Mat<T>::clear() {
  std::fill(m_storage.begin(), m_storage.end(), 0.);
}

/* -------------------------------------------------------------------------- */
template <typename T> void Mat<T>::resize(int m, int n) {
  m_size[1] = m;
  m_size[2] = n;
  m_storage.resize(m * n);
  // this->clear();
}
/* -------------------------------------------------------------------------- */
template <typename T> void Mat<T>::fill_num(T num) {
  for (auto &&element : this->m_storage) {
    element = num;
  }
}


  template <typename T> bool Mat<T>::verify(){
    double ret{0.0};
    for (auto &&element : this->m_storage) {
      ret+=element;
    }
    return (ret==16000000000);
  }
/* -------------------------------------------------------------------------- */
template class Mat<double>;
template class Mat<float>;
template class Mat<int>;
