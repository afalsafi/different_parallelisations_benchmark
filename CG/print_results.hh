#ifndef PRINT_RESULTS_HH
#define PRINT_RESULTS_HH

/* -------------------------------------------------------------------------- */
#include <algorithm>
#include <cmath>
#include <fstream>
#include <iostream>
#include <numeric>
#include <string>
#include <vector>

/* -------------------------------------------------------------------------- */
#include "algebra.hh"
#include "mat.hh"
/* -------------------------------------------------------------------------- */

void print(std::string title, const Vec_t &V) {
  std::cout << title << '\n';

  int n = V.size();
  for (int i = 0; i < n; i++) {
    double x = V[i];
    if (std::abs(x) < 1e-10)
      x = 0.0;
    std::cout << x << '\t';
  }
  std::cout << '\n';
}

/* -------------------------------------------------------------------------- */

void print(std::string title, const Mat_t &A) {
  std::cout << title << '\n';

  for (int i = 0; i < A.size()[0]; i++) {
    for (int j = 0; j < A.size()[1]; j++) {
      double x = A(i, j);
      if (std::abs(x) < 1e-10) {
        x = 0.0;
      }
      std::cout << x << '\t';
    }
    std::cout << '\n';
  }
}
#endif /*  PRINT_RESULTS_HH */
