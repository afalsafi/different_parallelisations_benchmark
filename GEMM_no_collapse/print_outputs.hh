#include <chrono>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <memory>
/* -------------------------------------------------------------------------- */
#include "mat.hh"
/* -------------------------------------------------------------------------- */

typedef std::chrono::high_resolution_clock clk;
typedef std::chrono::duration<double> second;
using Mat_t = Mat<double>;
/* -------------------------------------------------------------------------- */

void print_outputs(second time, int size1, int size2, std::string method,
                   Mat_t &C, int n_proc = 1) {

  std::cout << method << std::endl;
  std::cout << "nb of processors : " << n_proc << std::endl;
  std::cout << "matrix size1     : " << size1 << std::endl;
  std::cout << "matrix size2     : " << size2 << std::endl;
  std::cout << "compute time     : " << time.count() << " [s]" << std::endl;
  std::cout << "Performance      : "
            << (((double)size1 * (double)size2 * (double)size1 * 2.0) /
                ((time.count()) * 1.e9))
            << " [GF/s]" << std::endl;
  std::cout << "Verification     : " << C.verify(size1, size2) << std::endl
            << std::endl;
}

/* -------------------------------------------------------------------------- */
void print_outputs_file(second time, int size1, int size2, std::string method,
                        Mat_t &C, int n_proc = 1) {
  std::ofstream outfile;
  outfile.open("GEMM_output_" + method + "_size1_" + std::to_string(size1) +
                   "_size2_" + std::to_string(size2) + ".csv",
               std::ios::out | std::ios::app);
  outfile << std::setw(7) << method << ",";
  outfile << std::setw(11) << n_proc << ",";
  outfile << std::setw(9) << size1 << ",";
  outfile << std::setw(9) << size2 << ",";
  outfile << std::setw(9) << time.count() << ",";
  outfile << std::setw(9)
          << (((double)size1 * (double)size2 * (double)size1 * 2.0) /
              ((time.count()) * 1.e9))
          << ",";
  outfile << std::setw(9) << C.verify(size1, size2) << std::endl;
}

/* -------------------------------------------------------------------------- */
void print_header(int size1, int size2, std::string method,
                  bool if_print = false) {
  std::ofstream outfile;
  outfile.open("GEMM_output_" + method + "_size1_" + std::to_string(size1) +
                   "_size2_" + std::to_string(size2) + ".csv",
               std::ios::out);
  if (if_print) {
    outfile << std::setw(20) << "nb of procs,";
    outfile << std::setw(10) << "size1,";
    outfile << std::setw(10) << "size2,";
    outfile << std::setw(10) << "time,";
    outfile << std::setw(10) << "preformance,";
    outfile << std::setw(20) << "verification" << std::endl;
  }
  outfile.close();
}
