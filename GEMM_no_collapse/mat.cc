/* -------------------------------------------------------------------------- */
#include "mat.hh"
/* -------------------------------------------------------------------------- */
#include "/opt/intel/compilers_and_libraries_2019.5.281/linux/pstl/include/pstl/algorithm"
#include "/opt/intel/compilers_and_libraries_2019.5.281/linux/pstl/include/pstl/execution"
#include <cmath>
#include <iostream>
#include <numeric>
/* -------------------------------------------------------------------------- */

/* -------------------------------------------------------------------------- */
template <typename T>
Mat<T>::Mat(int m, int n) : m_size{m, n}, m_storage(m * n) {
  this->clear();
}

/* -------------------------------------------------------------------------- */
template <typename T>
Mat<T>::Mat(int m, int n, T num) : m_size{m, n}, m_storage(m * n) {
  this->fill_num(num);
}

/* -------------------------------------------------------------------------- */
template <typename T> void Mat<T>::clear() {
  std::fill(m_storage.begin(), m_storage.end(), 0.);
}

/* -------------------------------------------------------------------------- */
template <typename T> void Mat<T>::resize(int m, int n) {
  m_size[1] = m;
  m_size[2] = n;
  m_storage.resize(m * n);
  // this->clear();
}
/* -------------------------------------------------------------------------- */
template <typename T> void Mat<T>::fill_num(T num) {
  for (auto &&element : this->m_storage) {
    element = num;
  }
}

template <typename T> bool Mat<T>::verify(int size1, int size2) {
  auto ret =
      std::accumulate(this->m_storage.begin(), this->m_storage.end(), 0.0);
  return (ret == std::pow(double(m_storage.size()), 1.5) * 2.0 * size2 / size1);
}
/* -------------------------------------------------------------------------- */
template class Mat<double>;
template class Mat<float>;
template class Mat<int>;
