/* -------------------------------------------------------------------------- */
#include "/opt/intel/compilers_and_libraries_2019.5.281/linux/pstl/include/pstl/algorithm"
#include "/opt/intel/compilers_and_libraries_2019.5.281/linux/pstl/include/pstl/execution"
#include <chrono>
#include <iostream>
#include <memory>
#include <sstream>
#include <sys/time.h>
#include <tbb/tbb.h>
//#include <execution>
//#include <experimental/algorithm>
/* -------------------------------------------------------------------------- */
#include "mat.hh"
#include "print_outputs.hh"
/* -------------------------------------------------------------------------- */


/* -------------------------------------------------------------------------- */

int main(int /*argc*/, char *argv[]) {

  std::stringstream args(argv[1]);
  int N;
  args >> N;
  tbb::task_scheduler_init init(N);

  const int size = 2000;

  auto A = Mat_t(size, size, 1.0);
  auto B = Mat_t(size, size, 2.0);
  auto C = Mat_t(size, size, 0.0);

  // Matrix multiplication inline function
  auto _mul = [&](int i) {
    for (int j = 0; j < size; j++) {
      for (int k = 0; k < size; k++) {
        C(i, j) += A(i, k) * B(k, j);
      }
    }
  };

  std::array<int, size> nums;
  int count{0};
  for (auto &&num : nums) {
    num = count;
    count++;
  }

  // doing the loop with std::execution::par
  auto start = clk::now();
  std::for_each(std::execution::par, nums.begin(), nums.end(),
                [&](int &num) { _mul(num); });

  auto end = clk::now();
  second time = end - start;
  print_outputs(time, size, 1, "Execution policy", C);

  // doing the loop with std::execution::par_unseq
  A.fill_num(1.0);
  B.fill_num(2.0);
  C.fill_num(0.0);

  start = clk::now();
  std::for_each(std::execution::par_unseq, nums.begin(), nums.end(),
                [&](int &num) { _mul(num); });

  end = clk::now();
  time = end - start;
  print_outputs(time, size, 1, "Execution policy par_unseq", C);

  return 0;
}
