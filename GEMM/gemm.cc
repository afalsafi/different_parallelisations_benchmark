/* -------------------------------------------------------------------------- */
#include <chrono>
#include <iostream>
#include <memory>
#include <sstream>
#include <sys/time.h>
#include <tbb/tbb.h>
/* -------------------------------------------------------------------------- */
#include "mat.hh"
#include "print_outputs.hh"
/* -------------------------------------------------------------------------- */

int main(int /*argc*/, char ** /*argv[]*/) {

  int size = 200;

  auto A = Mat_t(size, size, 1.0);
  auto B = Mat_t(size, size, 2.0);
  auto C = Mat_t(size, size, 0.0);

  // Sequential loops
  auto &&start = clk::now();

  for (int i = 0; i < size; i++) {
    for (int j = 0; j < size; j++) {
      for (int k = 0; k < size; k++) {
        C(i, j) += A(i, k) * B(k, j);
      }
    }
  }

  auto &&end = clk::now();
  second time = end - start;
  print_outputs(time, size, 0, "Sequential", C);
  return 0;
}
