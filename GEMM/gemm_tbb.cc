/* -------------------------------------------------------------------------- */
#include <tbb/tbb.h>
#include <chrono>
#include <iostream>
#include <memory>
#include <sstream>
#include <sys/time.h>
/* -------------------------------------------------------------------------- */
#include "mat.hh"
#include "print_outputs.hh"
/* -------------------------------------------------------------------------- */

int main(int /*argc*/, char *argv[]) {

  std::stringstream args(argv[1]);
  int N;
  args >> N;

  tbb::task_scheduler_init init(N);

  int size = 2000;

  // std::unique_ptr<Mat<double>> A;
  auto A = Mat_t(size, size, 1.0);
  auto B = Mat_t(size, size, 2.0);
  auto C = Mat_t(size, size, 0.0);

  // Matrix multiplication inline function
  auto _mul = [&](int i) {
    for (int j = 0; j < size; j++) {
      for (int k = 0; k < size; k++) {
        C(i, j) += A(i, k) * B(k, j);
      }
    }
  };

  auto _mulij = [&](int i, int j) {
    for (int k = 0; k < size; k++) {
      C(i, j) += A(i, k) * B(k, j);
    }
  };

  // one parallel loop using TBB
  auto &&start = clk::now();
  start = clk::now();
  tbb::parallel_for(0, size, [&](int &i) { _mul(i); });
  auto &&end = clk::now();
  second time = end - start;
  print_outputs(time, size, 1, "thread building blocks", C);

  // two nested parallel loop using TBB
  A.fill_num(1.0);
  B.fill_num(2.0);
  C.fill_num(0.0);
  start = clk::now();
  tbb::parallel_for(0, size, [&](int i) {
    tbb::parallel_for(0, size, [&](int j) { _mulij(i, j); });
  });
  end = clk::now();
  time = end - start;
  print_outputs(time, size, 2, "thread building blocks", C);

  return 0;
}
