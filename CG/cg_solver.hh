/* -------------------------------------------------------------------------- */
#include <algorithm>
#include <chrono>
#include <cmath>
#include <fstream>
#include <iostream>
#include <numeric>
#include <vector>
/* -------------------------------------------------------------------------- */
#include "algebra.hh"
#include "mat.hh"
#include "print_outputs.hh"
#include "print_results.hh"

/* -------------------------------------------------------------------------- */

template <class Algebra_t>
void cg_solver(const Mat_t &A, const Vec_t &B, Algebra_t &alg,
               const double &eps, std::string method) {
  Vec_t X(B.size(), 1.0);
  int k{0};
  double alpha{0.0}, beta{0.0};
  auto &&start = clk::now();
  Vec_t R = alg.vec_comb(+1.0, alg.matrix_times_vector(A, X), -1.0, B);
  Vec_t P = alg.vec_comb(-1.0, alg.matrix_times_vector(A, X), +1.0, B);
  Vec_t Rold, AP;

  while (k < B.size()) {
    Rold = R; // Store previous residual
    AP = alg.matrix_times_vector(A, P);
    alpha = alg.inner_product(R, R);
    alpha = alpha / alg.inner_product(P, AP);

    X = alg.vec_comb(+1.0, X, +alpha, P);  // Next estimate of solution
    R = alg.vec_comb(+1.0, R, +alpha, AP); // Residual

    if (alg.vec_norm(R) < eps) {
      break;
    } // Convergence test

    beta = alg.inner_product(R, R) / alg.inner_product(Rold, Rold);
    P = alg.vec_comb(-1.0, R, beta, P); // Next gradient

    k++;
  }
  auto &&end = clk::now();
  second time = end - start;
  print_outputs_file(
      time, B.size(), method, k,
      alg.vec_norm(alg.vec_comb(1.0, alg.matrix_times_vector(A, X), -1.0, B)),
      alg.n_proc);
}
/* -------------------------------------------------------------------------- */
